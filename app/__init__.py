import os

from flask import Flask, request, jsonify, Response
from flask_sqlalchemy import SQLAlchemy
from configparser import ConfigParser

app = Flask(__name__)
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
# mysql config
config = ConfigParser()
config.read('config.ini')
host = config.get('mysql', 'host')
database = config.get('mysql', 'database')
user = config.get('mysql', 'user')
password = config.get('mysql', 'password')
app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql+pymysql://{}:{}@{}/{}'.format(user, password, host, database)
db = SQLAlchemy(app)

from app import controllers
