# interface
#
# uses
#   uuid
#   inventory
#   playbook
#   extra_vars
#
# does
#   run - runs ansible command, pushes each 256 chars of ansible output into the queue
#   kill - kills the ansible process, clears environment
#   read - gives output of ansible

from app.models import Inventory, Host, Variable
import os
import shortuuid
import datetime
import queue
import subprocess
import io


class Ansible():


    def __init__(self, inventory_id, playbook, extra_vars, log_name):
        # check for inventory exists
        inventory = Inventory.query.get(inventory_id)
        if not inventory:
            raise AnsibleException('inventory {} is absent'.format(inventory_id))
        self.inventory = inventory
        # check playbook
        if not os.path.exists('ansible/play/{}'.format(playbook)):
            raise AnsibleException('playbook {} is absent'.format(playbook))
        # generate ansible_id
        self.ansible_id = shortuuid.ShortUUID().random(length=10).lower()
        # save extra_vars in /ansible/run
        extra_vars_file = None
        if extra_vars:
            extra_vars_file = 'ansible/run/{}.{}'.format(self.ansible_id, 'extra')
            with open(extra_vars_file, 'w+') as f: f.write(str(extra_vars))
        self.extra_vars_file = extra_vars_file
        # save inventory in /ansible/run
        inventory_file = 'ansible/run/{}.{}.json'.format(self.ansible_id, 'hosts')
        self.inventory_file = inventory_file
        with open(inventory_file, 'w+') as f: f.write(str(self._dump_inventory()))
        # create log file in ansible/log
        if not log_name:
            log_name = '{}_{}.log'.format(playbook, datetime.now().strftime('%H_%M_%S'))
        log_file = open('ansible/log/{}'.format(log_name), 'w+')
        self.log_file = log_file
        # create cmd
        # cwd for ansible is ./ansible
        ansible_inventory = 'run/{}.{}.json'.format(self.ansible_id, 'hosts')
        ansible_extra_vars = 'run/{}.{}'.format(self.ansible_id, 'extra')
        self.cmd = 'ansible-playbook play/{} -i {} --extra-vars=\'@{}\''.format(playbook, ansible_inventory, ansible_extra_vars)
        # create queue
        self.queue = queue.Queue()
        # init ansible
        self.ansible = None
        self.exit_code = None

    def _dump_inventory(self):
        hosts = {}
        for host in Host.query.filter_by(inventory_id=self.inventory.id).all():
            vars_d = {}
            for variable in Variable.query.filter_by(host_id=host.id).all():
                vars_d[variable.name] = variable.value
            hosts[host.name] = vars_d
        return {'all' : {'hosts' : hosts}}


    def play(self):
        # running ansible
        self.ansible = subprocess.Popen(self.cmd, shell=True, executable='/bin/bash', stdout=subprocess.PIPE, stderr=subprocess.STDOUT, cwd="ansible")
        reader = io.TextIOWrapper(self.ansible.stdout)
        while True:
            data = reader.read(256)
            if not data:
                break
            self.queue.put_nowait(data)
            self.log_file.write(data)
        # finished ansible
        self.ansible.wait()
        self.exit_code = self.ansible.poll()
        self._clear_env()


    def kill(self):
        if self.ansible:
            self.ansible.kill()
            self.exit_code = self.ansible.poll()
            self._clear_env()


    def _clear_env(self):
        # close files
        # remove files
        self.log_file.close()
        try:
            if self.extra_vars_file:
                os.remove(self.extra_vars_file)
            os.remove(self.inventory_file)
        except Exception as e:
            pass


    def read(self, timeout):
        # if self.queue.qsize() > 0
            # do self.queue.qsize() times queue.get() and build the final string
        # else
        # wait for data for timeout
        # queue.get(block=True, timeout=timeout)
        output = ''
        if self.queue.qsize() > 0:
            for i in range(self.queue.qsize()):
                output = output + self.queue.get_nowait()
            return output
        output = self.queue.get(timeout=timeout)
        return output


    def is_running(self):
        return self.ansible.poll() is None


class AnsibleException(Exception):
    pass

