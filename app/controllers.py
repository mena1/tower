import json
import shortuuid
import os
import subprocess
import io
import magic
import jsonschema

from concurrent.futures import ThreadPoolExecutor
from app import request, jsonify, Response
from app.models import Inventory, Host, Variable
from app.ansible import Ansible, AnsibleException
from app import app

errors = {
    0 : 'your passed invalid json data. see json scheme for {endpoint}',
    1 : 'resource {resource} with id {id} not found',
    2 : 'json expected',
    3 : 'resource {resource} with id {id} already exists'
}

ansible_instances = {}

def error(code, **kwargs):
    return jsonify({'success' : False, 'error' : errors[code].format(**kwargs)})

def error_with_msg(msg):
    return jsonify({'success' : False, 'error' : msg})

"""
    POST /run

    put ansible instance into ANSIBLE_INSTANCES

    run ansible asynchronously

    response with ansible id
"""
@app.route('/run', methods=['POST'])
def run():
    request_data = request.get_json(force=True, silent=True)
    if request_data == None:
        return error(2)
    schema = {
        "type" : "object",
        "properties" : {
            "inventory_id" : {"type" : "number"},
            "playbook" : {"type" : "string"},
            "log" : {"type" : "string"},
            "extra_vars" : {"type" : "object"}
        },
        "required" : ["inventory_id", "playbook"]
    }
    try:
        jsonschema.validate(schema=schema, instance=request_data)
    except Exception as e:
        return error(0, endpoint='POST /prepare')
    try:
        ansible = Ansible(inventory_id=request_data['inventory_id'], playbook=request_data['playbook'], log_name=request_data.get('log_name', None), extra_vars=request_data.get('extra_vars', None))
        ansible_instances[ansible.ansible_id] = ansible
        executor = ThreadPoolExecutor(1)
        executor.submit(ansible.play)
        return jsonify({'success': True, 'ansible_id': ansible.ansible_id})
    except AnsibleException as e:
        return error_with_msg(str(e))
"""
    GET /data?ansible_id=<ansible_id>&timeout=<seconds> reads new data from ansible

    get ansible with ansible_id

    read new stdout data with timeout

    if ansible is not finished response with
        if data is present:
            running: yes, data: string
        otherwise:
            running: yes, data: null

    else
        running: no, data: string, ansible_code: 0|1

    if client got exit_code, it means that ansible is finished and all data already is read!
"""
@app.route('/data', methods=['GET'])
def data():
    ansible_id = request.args.get('ansible_id', default = None)
    ansible = ansible_instances.get(ansible_id)
    if ansible:
        try:
            output = ansible.read(timeout=request.args.get('timeout', default = None))
        except Exception as e:
            output = None
        if ansible.exit_code != None:
            del ansible_instances[ansible_id]
        return jsonify({'success': True,'data': output, 'exit_code': ansible.exit_code})
    return error(1, resource='ansible instance', id=ansible_id)
"""
    GET /kill?ansible_id=<ansible_id>

    get ansible with uuid

    ansible.kill()

"""
@app.route('/kill', methods=['GET'])
def kill():
    ansible_id = request.args.get('ansible_id', default = None)
    ansible = ansible_instances.get(ansible_id)
    if ansible:
        ansible.kill()
        del ansible_instances[ansible_id]
        return jsonify({'success': True})
    return error(1, resource='ansible instance', id=ansible_id)

@app.route('/ansible/<directory>', methods=['GET'])
def get_files_from(directory):
    if directory not in ['ssh', 'extra', 'log', 'play', 'file']:
        return error(1, resource='directory', id=directory )
    files = []
    for file in os.listdir(os.path.join(ansible_dir, directory)):
        if os.path.isfile(os.path.join(ansible_dir, directory, file)):
            files.append(file)
    return jsonify({'success' : True, 'files' : files})

ansible_dir = 'ansible'

@app.route('/ansible/<directory>/<name>', methods=['GET'])
def get_file(directory,name):
    if directory not in ['ssh', 'extra', 'log', 'play', 'file']:
        return error(1, resource='directory', id=directory)
    if not os.path.isfile(os.path.join(ansible_dir, directory, name)):
        return error(1, resource='file', id=name)
    f = os.path.join(ansible_dir, directory, name)
    content = open(f, 'r').read()
    mime = magic.Magic(mime=True)
    return Response(content, mimetype=mime.from_file(f))


@app.route('/ansible/<directory>', methods=['POST'])
def post_file(directory):
    if directory not in ['ssh', 'extra', 'log', 'play', 'file']:
        return error(1, resource='directory', id=directory)
    if 'file' not in request.files:
        return jsonify({'success' : True})
    file = request.files['file']
    if file.filename == '':
        return jsonify({'success' : True})
    if os.path.isfile(os.path.join(ansible_dir, directory, file.filename)):
        return error(3, resource='file', id='/ansible/{}/{}'.format(directory, file.filename))
    file.save(os.path.join(ansible_dir, directory, file.filename))
    return jsonify({'success': True, 'uri' : '/ansible/{}/{}'.format(directory, file.filename)})


@app.route('/ansible/<directory>/<name>', methods=['DELETE'])
def delete_file(directory, name):
    if directory not in ['ssh', 'extra', 'log', 'play', 'file']:
        return error(1, resource='directory', id=directory)
    if not os.path.isfile(os.path.join(ansible_dir, directory, name)):
        return error(1, resource='file', id=name)
    os.remove(os.path.join(ansible_dir, directory, name))
    return jsonify({'success': True})


# INVENTORY
@app.route('/inventory', methods=['GET'])
def get_inventories():
    inventories = Inventory.query.all()
    l = []
    if inventories == None:
        inventories = []
    for i in inventories:
        l.append({'id' : i.id, 'name' : i.name})
    return jsonify({'success' : True, 'inventories' : l})


@app.route('/inventory/<int:id>', methods=['GET'])
def get_inventory(id):
    i = Inventory.query.get(id)
    if i != None:
        return jsonify({'success' : True, 'inventory' : {'id': i.id, 'name' : i.name}})
    return error(1, resource='inventory', id=id)


@app.route('/inventory', methods=['POST'])
def post_inventory():
    request_data = request.get_json(force=True, silent=True)
    if request_data == None:
        return error(2)
    schema = {
        "type" : "object",
        "properties" : {
            "name" : {"type" : "string", "pattern" : "^.+$"}
        },
        "required" : ["name"]
    }
    try:
        jsonschema.validate(schema=schema, instance=request_data)
    except Exception as e:
        return error(0, endpoint='POST /inventory')
    i = Inventory(name=request_data['name'])
    db.session.add(i)
    db.session.commit()
    return jsonify({'success' : True, 'inventory' : {'id' : i.id, 'name' : i.name}})

@app.route('/inventory/<int:id>', methods=['PUT'])
def update_inventory(id):
    request_data = request.get_json(force=True, silent=True)
    if request_data == None:
        return error(2)
    schema = {
        "type" : "object",
        "properties" : {
            "name" : {"type" : "string", "pattern" : "^.+$"}
        },
        "required" : ["name"]
    }
    try:
        jsonschema.validate(schema=schema, instance=request_data)
    except Exception as e:
        return error(0, endpoint='PUT /inventory')
    inventory = Inventory.query.get(id)
    if not inventory:
        return error(1, resource='inventory', id=id)
    inventory.name = request_data['name']
    db.session.commit()
    return jsonify({'success' : True, 'inventory' : {'id' : inventory.id, 'name' : inventory.name}})


@app.route('/inventory/<int:id>', methods=['DELETE'])
def delete_inventory(id):
    inventory = Inventory.query.get(id)
    if not inventory:
        return error(1, resource='inventory', id=id)
    db.session.delete(inventory)
    db.session.commit()
    return jsonify({'success' : True})


# HOSTS
#@app.route('/host', methods=['GET'])
#def get_hosts():
#    query = {}
#    try:
#        if request.args.get('query'):
#            query = json.loads(request.args.get('query'))
#    except Exception as e:
#        return jsonify({'success' : '0', 'error' : 'query json is not valid'})
#    schema = {
#        "type" : "object",
#        "properties" : {
#            "inventory_id" : {"type" : "number"}
#        },
#        "required" : ["inventory_id"]
#    }
#    try:
#        jsonschema.validate(schema=schema, instance=query)
#    except Exception as e:
#        return jsonify({'success' : 0, 'error' : 'query json and schema '})
#    hosts = Host.query.filter_by(**query).all()
#    l = []
#    for h in hosts:
#        l.append({'id' : h.id, 'name' : h.name, 'inventory_id' : h.inventory_id})
#    return jsonify({'success' : '1', 'hosts' : l})

@app.route('/inventory/<int:id>/host')
def get_hosts(id):
    i = Inventory.query.get(id)
    if i == None:
        return error(1, resource='inventory', id=id)
    hosts = Host.query.filter_by(inventory_id=id).all()
    l = []
    for h in hosts:
        l.append({'id' : h.id, 'name' : h.name, 'inventory_id' : h.inventory_id})
    return jsonify({'success' : True, 'hosts' : l})

@app.route('/host/<int:id>', methods=['GET'])
def get_host(id):
    h = Host.query.get(id)
    if h != None:
        return jsonify({'success' : True, 'host' : {'id': h.id, 'name' : h.name, 'inventory_id' : h.inventory_id}})
    return error(1, resource='host', id=id)


@app.route('/host', methods=['POST'])
def post_host():
    request_data = request.get_json(force=True, silent=True)
    if request_data == None:
        return error(2)
    schema = {
        "type" : "object",
        "properties" : {
            "name" : {"type" : "string", "pattern" : "^.+$"},
            "inventory_id" : {"type" : "number"}
        },
        "required" : ["name", "inventory_id"]
    }
    try:
        jsonschema.validate(schema=schema, instance=request_data)
    except Exception as e:
        return error(0, endpoint='POST /host')
    h = Host(name=request_data['name'], inventory_id=request_data['inventory_id'])
    db.session.add(h)
    db.session.commit()
    return jsonify({'success' : True, 'host' : {'id' : h.id, 'name' : h.name, 'inventory_id' : h.inventory_id}})


@app.route('/host/<int:id>', methods=['PUT'])
def update_host(id):
    request_data = request.get_json(force=True, silent=True)
    if request_data == None:
        return error(2)
    schema = {
        "type" : "object",
        "properties" : {
            "name" : {"type" : "string", "pattern" : "^.+$"}
        },
        "required" : ["name"]
    }
    try:
        jsonschema.validate(schema=schema, instance=request_data)
    except Exception as e:
        return error(0, endpoint='PUT /host')
    host = Host.query.get(id)
    if host == None:
        return error(1, resource='host', id=id)
    host.name = request_data.json['name']
    db.session.commit()
    return jsonify({'success' : True, 'host' : {'id' : host.id, 'name': host.name, 'inventory_id' : host.inventory_id}})


@app.route('/host/<id>', methods=['DELETE'])
def delete_host(id):
    host = Host.query.get(id)
    if not host:
        return error(1, resource='host', id=id)
    db.session.delete(host)
    db.session.commit()
    return jsonify({'success' : True})


# VARIABLES
#@app.route('/var', methods=['GET'])
#def get_vars():
#    query = {}
#    try:
#        if request.args.get('query'):
#            query = json.loads(request.args.get('query'))
#    except Exception as e:
#        return jsonify({'success' : '0', 'error' : 'query json is not valid'})
#    schema = {
#        "type" : "object",
#        "properties" : {
#            "host_id" : {"type" : "number"}
#        },
#        "required" : ["host_id"]
#    }
#    try:
#        jsonschema.validate(schema=schema, instance=query)
#    except Exception as e:
#        return jsonify({'success' : 0, 'error' : 'query json and schema '})
#    vars = Variable.query.filter_by(**query).all()
#    l = []
#    for v in vars:
#        l.append({'id' : v.id, 'name' : v.name, 'value' : v.value, 'host_id' : v.host_id})
#    return jsonify({'success' : '1', 'vars' : l})

@app.route('/host/<int:id>/var')
def get_vars(id):
    h = Host.query.get(id)
    if not h:
        return error(1, resource='host', id=id)
    _vars = Variable.query.filter_by(host_id=id).all()
    l = []
    for v in _vars:
        l.append({'id' : v.id, 'name' : v.name, 'value' : v.value, 'host_id' : v.host_id})
    return jsonify({'success' : True, 'vars' : l})

@app.route('/var/<int:id>', methods=['GET'])
def get_var(id):
    v = Variable.query.get(id)
    if not v:
        return error(1, resource='var', id=id)
    return jsonify({'success' : '1', 'var' : {'id': v.id, 'name' : v.name, 'value' : v.value, 'host_id' : v.host_id}})

@app.route('/var', methods=['POST'])
def post_var():
    request_data = request.get_json(force=True, silent=True)
    if request_data == None:
        return error(2)
    schema = {
        "type" : "object",
        "properties" : {
            "name" : {"type" : "string", "pattern" : "^.+$"},
            "value" : {"type" : "string", "pattern" : "^.+$"},
            "host_id" : {"type" : "number"}
        },
        "required" : ["name", "value", "host_id"]
    }
    try:
        jsonschema.validate(schema=schema, instance=request_data)
    except Exception as e:
        return error(0, endpoint='POST /var')
    v = Variable(name=request.json['name'], value=request.json['value'], host_id=request.json['host_id'])
    db.session.add(v)
    db.session.commit()
    return jsonify({'success' : True, 'var' : {'id' : v.id, 'name' : v.name, 'value' : v.value, 'host_id' : v.host_id}})


@app.route('/var/<int:id>', methods=['PUT'])
def update_var(id):
    request_data = request.get_json(force=True, silent=True)
    if request_data == None:
        return error(2)
    schema = {
        "type" : "object",
        "properties" : {
            "name" : {"type" : "string", "pattern" : "^.+$"},
            "value" : {"type" : "string", "pattern" : "^.+$"}
        },
        "required" : ["name", "value"]
    }
    try:
        jsonschema.validate(schema=schema, instance=request_data)
    except Exception as e:
        return error(0, endpoint='PUT /var')
    var = Variable.query.get(id)
    if var == None:
        return error(1, resource='var', id=id)
    var.name = request.json['name']
    var.value = request.json['value']
    db.session.commit()
    return jsonify({'success' : True, 'var' : {'id' : var.id, 'name' : var.name, 'value' : var.value, 'host_id' : var.host_id}})


@app.route('/var/<id>', methods=['DELETE'])
def delete_var(id):
    var = Variable.query.get(id)
    if not var:
        return error(1, resource='var', id=id)
    db.session.delete(var)
    db.session.commit()
    return jsonify({'success' : True})

